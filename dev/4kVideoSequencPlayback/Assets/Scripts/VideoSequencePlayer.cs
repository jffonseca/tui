﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;


namespace MediaPlayer
{
    public class VideoSequencePlayer : MonoBehaviour
    {
        public List<string> videoFiles;

        private List<Video> v;

        private int videoClipIndex;
        private bool nextVideoIsLoaded;


        void Start()
        {
            nextVideoIsLoaded = false;

            loadPathsFromFolder();

            videoClipIndex = 0;
            StartCoroutine(loadNextVideo(videoClipIndex));
            //loadNextVideo();

            // set texture for the shader
            GetComponent<Renderer>().material.mainTexture = v[videoClipIndex].playbackTexture;
            v[videoClipIndex].Play();
        }



        void Update()
        {

            if ((v[videoClipIndex].GetPosition() >= v[videoClipIndex].GetDuration() / 2) && !nextVideoIsLoaded)
            {
                StartCoroutine(loadNextVideo( (videoClipIndex+1)%videoFiles.Count) );
                nextVideoIsLoaded = true;
            }


            if ((v[videoClipIndex].GetPosition() >= v[videoClipIndex].GetDuration()-1) && nextVideoIsLoaded)
            {
                videoClipIndex++;
                videoClipIndex = videoClipIndex % videoFiles.Count;

                // set texture for the shader
                GetComponent<Renderer>().material.mainTexture = v[videoClipIndex].playbackTexture;
                v[videoClipIndex].Play();
                nextVideoIsLoaded = false;
            }



            Debug.Log("video "+ videoClipIndex+" " +v[videoClipIndex].GetPosition()+"/"+ v[videoClipIndex].GetDuration());

        }


        void loadPathsFromFolder()
        {

             v = new List<Video>();

            for (int i = 0; i < videoFiles.Count; i++) {

                Video newVid = new Video(i, (Application.streamingAssetsPath + "/" + videoFiles[i]).Replace("/", "\\"));
                v.Add(newVid);
                StartCoroutine(v[v.Count - 1].CallPluginAtEndOfFrames());
            }


        }


       IEnumerator loadNextVideo(int _vIndex) {
    
            v[_vIndex].Load();
         
            yield return null;
       }

    }
}
