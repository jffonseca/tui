﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RenderShaderToTexture : MonoBehaviour {

    public RenderTexture texture;
    public Material mat;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


     

        Vector2 m = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
        mat.SetVector("_MousePos", m);

        // Debug.Log(mat.name);
        var scratchRT = RenderTexture.GetTemporary(texture.width, texture.height, 0, texture.format, RenderTextureReadWrite.sRGB);
        scratchRT.filterMode = texture.filterMode;

       // texture = scratchRT;
        RenderTexture.active = scratchRT;
        GL.Clear(true, true, Color.blue);
        RenderTexture.active = null;

        Graphics.Blit(scratchRT, texture,mat);
        RenderTexture.ReleaseTemporary(scratchRT);
    }
}
