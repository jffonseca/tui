﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RawImagFullscreen : MonoBehaviour {

    public Texture backgroundTexture;

    void OnGUI () {

    
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture, ScaleMode.StretchToFill); // ScaleMode.ScaleToFit
    }
	
	// Update is called once per frame
	void Update () {
    
       
    }
}
