﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class MeshDeformerInput : MonoBehaviour {

    public float force = 20f;
    public float forceOffset = 0.1f;
    MeshDeformer[] meshDeformer;
    Transform colObject1;
    bool hitMoment = false;


    void Start()
    {
        colObject1 = GetComponent<Transform>();
    }

    void Update()
    {

            if (hitMoment == true)
            {
                HandleInput();
            }
     }
    

    void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "orgObject")
        {
            hitMoment = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "orgObject")
        {
            hitMoment = false;
        }
    }

    void HandleInput()
    {
 
        Ray inputRay = new Ray(colObject1.position, Camera.main.transform.position - colObject1.position);
		RaycastHit hit;

		if (Physics.Raycast(inputRay, out hit))
        {
            Debug.DrawLine(inputRay.origin, hit.point);

           MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();

            if (deformer)
            {
				Vector3 point = hit.point;
		    	point += hit.normal * forceOffset;
				deformer.AddDeformingForce(point, force);
			}
         
		}

    }
}