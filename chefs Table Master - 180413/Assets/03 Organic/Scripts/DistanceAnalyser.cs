﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceAnalyser : MonoBehaviour
{
    float dist_colToOrgObj;
    float pufferDistance = 0.1f;
    BoxCollider boxCollider;
    GameObject[] colObjects;
    public float delayAmnt;
    float timer = 0;

    void Start()
    {     
       boxCollider =  gameObject.GetComponent<BoxCollider>();
       ///Maximum Distance From Collider Center to Collider bound
       float maxLength_CenterToBound_clObj = (Mathf.Max(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z))/2;
       dist_colToOrgObj = maxLength_CenterToBound_clObj + GameObject.FindWithTag("ColliderObjects").GetComponent<SphereCollider>().radius + pufferDistance;
       colObjects = GameObject.FindGameObjectsWithTag("ColliderObjects");
    }
    

    void Update()
    {

        int checker = 0;

        foreach (GameObject colObject in colObjects) 
     
        {       
                float dist = Vector3.Distance(colObject.transform.position, boxCollider.center);    
                if (dist < dist_colToOrgObj)
                {           
                checker++;
                }
        }

        if (checker > 0)
        {
            timer = 0;
            this.gameObject.GetComponent<MeshDeformer>().enabled = true;
            //For Debugging
            //this.gameObject.GetComponent<MeshRenderer>().enabled = true;
        }

        else
        {
            timer += Time.deltaTime;
            if (timer > 1.5f)
             { 
             this.gameObject.GetComponent<MeshDeformer>().enabled = false;
                //For Debugging
                //  this.gameObject.GetComponent<MeshRenderer>().enabled = false;
                timer = 0;  ///kann auch weg
             }

        }
        
    }
}

