﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugManager : MonoBehaviour {

    public GameObject[] debugObjects;

    [SerializeField]
    private CanvasGroup canvasGroup;

    // Use this for initialization
    void Start () {
        hideDebug();
	}
	
	// Update is called once per frame
	void Update ()
    {
        keyStroke();
	}

    void keyStroke()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            showDebug();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            hideDebug();
        }

    }

    void showDebug()
    {

        //if (debugObjects.Length > 0)
        //{
            for (int i = 0; i < debugObjects.Length; i++)
            {
                debugObjects[i].GetComponent<Renderer>().enabled = true;
            }
        //}

        canvasGroup.alpha = 1f;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;

    }

    void hideDebug()
    {
        for (int i = 0; i < debugObjects.Length; i++)
        {
            debugObjects[i].GetComponent<Renderer>().enabled = false;
        }

        canvasGroup.alpha = 0f;
        canvasGroup.interactable =  false;
        canvasGroup.blocksRaycasts = false;

    }
}

