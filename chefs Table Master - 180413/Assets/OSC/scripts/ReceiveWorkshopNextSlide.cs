﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveWorkshopNextSlide : MonoBehaviour {
    

    public OSC osc;
	

	// Use this for initialization
	void Start () {

		Debug.Log("OSC Started");
		osc.SetAddressHandler("/workshop/workshopNextSlide", OnReceiveWorkshopNextSlide);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveWorkshopNextSlide(OscMessage message){
		
		Debug.Log("NextSlide");
		//scene.changeScene(index);


        LoadAndDisplayContent d = GameObject.Find("RawImage").GetComponent<LoadAndDisplayContent>();
        d.nextSlide();
 
    }
}
