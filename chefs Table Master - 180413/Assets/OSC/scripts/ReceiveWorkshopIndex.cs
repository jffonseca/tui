﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveWorkshopIndex : MonoBehaviour {
    

    public OSC osc;
	

	// Use this for initialization
	void Start () {

		Debug.Log("OSC Started");
		osc.SetAddressHandler("/workshop/workshopIndex", OnReceiveWorkshopIndex);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveWorkshopIndex(OscMessage message){
		int index = message.GetInt(0);
		//Debug.Log("Changing Scene: " + index );
		//scene.changeScene(index);


        LoadAndDisplayContent d = GameObject.Find("RawImage").GetComponent<LoadAndDisplayContent>();

        d.upateWorkshopIndexFromOSC(index);
        //d.workshopIndex = (d.Workshop)0; //System.Enum.Parse(typeof(Workshop),index);
        // Debug.Log();
    }
}
