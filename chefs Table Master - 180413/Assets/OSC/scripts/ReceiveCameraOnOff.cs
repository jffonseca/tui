using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveCameraOnOff : MonoBehaviour {

	public OSC osc;
	public toggleVisible toggle;
	//public bool activeOnStart = false;
	//public GameObject[] objectsArray;

	// Use this for initialization
	void Start () {
		osc.SetAddressHandler("/ChefsTable/BlackMagic", OnReceiveCameraOnOff);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveCameraOnOff(OscMessage message){
		int onoff = message.GetInt(0);
		Debug.Log("BlackMagic View: " + onoff );
		toggle.toggle();
	}

	/*private void toggle () {
				activeOnStart = !activeOnStart;
				for (int i = 0; i<objectsArray.Length; i++)
				{
					objectsArray[i].SetActive(activeOnStart);
				}
		 }*/
}
