using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ReceiveCameraOnOffRawImage : MonoBehaviour {

	public OSC osc;

	public GameObject img;

	private bool state;
	//public bool activeOnStart = false;
	//public GameObject[] objectsArray;

	// Use this for initialization
	void Start () {
		state = false;
		img.SetActive(state);
	
		osc.SetAddressHandler("/ChefsTable/BlackMagic", OnReceiveCameraOnOff);
	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveCameraOnOff(OscMessage message){
		int onoff = message.GetInt(0);
		Debug.Log("BlackMagic View: " + onoff );
		//toggle.toggle();
		toggleState();
	}
	public void toggleState(){
		state=!state;

		img.SetActive(state);
		Debug.Log ("asdasdsa");
	}
	/*private void toggle () {
				activeOnStart = !activeOnStart;
				for (int i = 0; i<objectsArray.Length; i++)
				{
					objectsArray[i].SetActive(activeOnStart);
				}
		 }*/
}
