﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveWorkshopReset : MonoBehaviour {
    

    public OSC osc;
	

	// Use this for initialization
	void Start () {

		Debug.Log("OSC Started");
		osc.SetAddressHandler("/workshop/workshopReset", OnReceiveWorkshopPreviousSlide);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveWorkshopPreviousSlide(OscMessage message){
		int index = message.GetInt(0);
		//Debug.Log("Changing Scene: " + index );
		//scene.changeScene(index);


        LoadAndDisplayContent d = GameObject.Find("RawImage").GetComponent<LoadAndDisplayContent>();
        d.resetWorkshop();

    }
}
