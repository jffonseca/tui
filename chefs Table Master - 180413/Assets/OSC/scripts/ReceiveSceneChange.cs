﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveSceneChange : MonoBehaviour {

	public OSC osc;
	public sceneController scene;

	// Use this for initialization
	void Start () {

		Debug.Log("OSC Started");
		osc.SetAddressHandler("/ChefsTable/changeScene", OnReceiveSceneIndex);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnReceiveSceneIndex(OscMessage message){
		int index = message.GetInt(0);
		Debug.Log("Changing Scene: " + index );
		scene.changeScene(index);
	}
}
