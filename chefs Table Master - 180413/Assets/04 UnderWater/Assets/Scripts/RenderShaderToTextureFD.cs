﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RenderShaderToTextureFD : MonoBehaviour {

    public RenderTexture InteractionTexture;
    public RenderTexture texture;
    private RenderTexture buffer;
    public Material mat;



    // Use this for initialization
    void Start()
    {

        buffer = new RenderTexture(texture.width, texture.height, 0, texture.format, RenderTextureReadWrite.sRGB);
        buffer.filterMode = texture.filterMode;


        RenderTexture.active = texture;
        GL.Clear(true, true, Color.black);
        RenderTexture.active = null;

        // texture = scratchRT;
        RenderTexture.active = buffer;
		GL.Clear(true, true, Color.black);
        RenderTexture.active = null;

        
    }

    // Update is called once per frame
    void Update()
    {

        mat.SetTexture("_InteractionTex", InteractionTexture);
        mat.SetTexture("_OldTex", buffer);
        // Debug.Log(mat.name);
        var scratchRT = RenderTexture.GetTemporary(texture.width, texture.height, 0, texture.format, RenderTextureReadWrite.sRGB);
        scratchRT.filterMode = texture.filterMode;
        
        Graphics.Blit(scratchRT, texture, mat);
        //   Graphics.Blit(InteractionTexture, buffer);
        RenderTexture.ReleaseTemporary(scratchRT);

        Graphics.Blit(InteractionTexture, buffer);


    }
}
