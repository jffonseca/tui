﻿Shader "Unlit/displacement"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DisplacementTex("DisplacementTexture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
		//	#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			
			sampler2D _DisplacementTex;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float amplitude = 0.03;
				float4 disp = (tex2D(_DisplacementTex, i.uv))*amplitude;
			    float2 d = float2( (1- amplitude/2) + (i.uv.x + disp.x),1- ((1- amplitude/2)+ (i.uv.y + disp.y)));



				fixed4 col = tex2D(_MainTex, d);

				float3 light = normalize(float3(1,1,1));
			
				float spec = pow(max(0., -reflect(light, disp*10).z), 32);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				//return col;
				return (col );
			}
			ENDCG
		}
	}
}
