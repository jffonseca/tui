﻿Shader "Unlit/FrameDiff"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_InteractionTex("Interaction Texture", 2D) = "white" {}
		_BlurAmount("Blur Amount", Range(0,02)) = 0.0005
		
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
		
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
		//		UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _OldTex;
			sampler2D _MainTex;
			sampler2D _InteractionTex;

			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			//	UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float4 blend(float4 base, float4 blend) {
				return base + blend - 2.0 * blend;

			}

			float4 diff(float4 base, float4 blend) {
				return abs(base - blend);

			}

			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture

				float2 iuv = float2(i.uv.x, 1-i.uv.y);
				float4 oldFrame = tex2D(_OldTex, iuv);
				float4 newFrame = tex2D(_InteractionTex, iuv);
				// apply fog
				//UNITY_APPLY_FaOG(i.fogCoord, col);
				if(diff(newFrame, oldFrame).r > 0.95)return float4(diff(newFrame, oldFrame).rgb,1);//float4((newFrame - oldFrame).rgb, 1);//float4(blend(newFrame, oldFrame).rgb,1);
				else return 0;
			}

			

			ENDCG
		}
	
	}
}
