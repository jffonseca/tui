﻿Shader "VanTa/Unlit/imageTransform"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Transform ("Transform", Vector) = (1,1,0,0)
		_Brightness ("Brightness", Float) = 0.0
		_Contrast ("Contrast", Float) = 1.0
		_Gamma ("Gamma", Float) = 1.0
		_InBlack ("in Black", Vector) = (0,0,0,0)
		_OutBlack ("out Black", Vector) = (1,1,1,1)
		 _InWhite ("in White", Vector) = (0,0,0,0)
		 _OutWhite ("out White", Vector) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Overlay" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			//#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Transform;
			float _Brightness;
			float _Contrast;
			float _Gamma;
			float4 _Channels;
			float4 _InBlack;
			float4 _OutBlack;
			float4 _InWhite;
			float4 _OutWhite;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float levelChannel(float inPixel, float inBlack, float inGamma, float inWhite, float outBlack, float outWhite){
				return (pow( ((inPixel * 255.0) - inBlack)/(inWhite-inBlack), inGamma ) * (outWhite - outWhite) + outBlack) / 255.0;
			}

			float levelChannel2(float inPixel, float inBlack, float inGamma, float inWhite, float outBlack, float outWhite){
				inPixel = min(max(inPixel - inBlack, 0.0f) / (outBlack - inBlack), 1.0f);
				inPixel = pow(inPixel, inGamma);
				inPixel = lerp(inWhite, outWhite, inPixel);

				return inPixel;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 uv = i.uv;
				uv.x *= _Transform.x;
				uv.y *= _Transform.y;
				uv.x += _Transform.z;
				uv.y += _Transform.w;
				// sample the texture
				fixed4 col = tex2D(_MainTex, uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				fixed4 contrasted = (col - 0.5) * _Contrast + 0.5 + _Brightness;
				//return pow(contrasted, fixed4(_Gamma, _Gamma, _Gamma, _Gamma));


				fixed4 output = fixed4(1.0, 1.0, 1.0, 1.0);
				output.r = levelChannel2(contrasted.r, _InBlack.r, _Gamma, _InWhite.r, _OutBlack.r, _OutWhite.r);
				output.g = levelChannel2(contrasted.g, _InBlack.g, _Gamma, _InWhite.g, _OutBlack.g, _OutWhite.g);
				output.b = levelChannel2(contrasted.b, _InBlack.b, _Gamma, _InWhite.b, _OutBlack.b, _OutWhite.b);
				return output;
			}
			ENDCG
		}
	}
}
