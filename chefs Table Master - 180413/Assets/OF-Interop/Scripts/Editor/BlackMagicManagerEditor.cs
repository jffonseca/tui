﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(blackMagicManager))]
public class blackMagicManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        blackMagicManager pluginManager = (blackMagicManager)target;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Black Magic Resolution Mode");
        EditorGUILayout.Separator();


        if (GUILayout.Button("1080p24"))
        {
            pluginManager.setResolution(1);
        }

        if (GUILayout.Button("1080p25"))
        {
            pluginManager.setResolution(2);
        }

        if (GUILayout.Button("1080p50"))
        {
            pluginManager.setResolution(3);
        }

        if (GUILayout.Button("720p50"))
        {
            pluginManager.setResolution(4);
        }

        if (GUILayout.Button("720p60"))
        {
            pluginManager.setResolution(5);
        }
    }
}
