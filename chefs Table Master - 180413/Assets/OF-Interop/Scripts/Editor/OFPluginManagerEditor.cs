﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OFPluginManager))]
public class OFPluginManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        OFPluginManager pluginManager = (OFPluginManager) target;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Open Frameworks Plugin Controls");
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Objects detected: ", pluginManager.ObjectsCount.ToString());
        EditorGUILayout.Separator();
       

        //change song

        if (GUILayout.Button("Reset Background"))
        {
            pluginManager.resetBackground();
        }
    }
}
