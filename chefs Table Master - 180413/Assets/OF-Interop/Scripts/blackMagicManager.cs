﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;


public class blackMagicManager : MonoBehaviour
{

    public Texture2D _texture;
    private byte[] _processedImage;
    private int cameraWidth = 1920;
    private int cameraHeight = 1080;
    private int bytesperPixel = 4;
    private bool changingResolution = false;

    void Start()
    {
        //setResolution(4);
        OFInterop.blackMagic_Setup();
        Renderer _renderer = GetComponent<Renderer>();
        _texture = new Texture2D(cameraWidth, cameraHeight, TextureFormat.RGBA32, false);
        _renderer.material.mainTexture = _texture;

    }

    void Update()
    {
        if (changingResolution == false)
        {
            OFInterop.blackMagic_Update();

            _processedImage = null;
            _processedImage = new byte[cameraWidth * cameraHeight * bytesperPixel];
            unsafe
            {
                fixed (byte* image = _processedImage)
                {
                    OFInterop.blackMagig_getImage(image);
                }
            }
            _texture.LoadRawTextureData(_processedImage);
            _texture.Apply();
        }

    }


    /*****************************
     * 1080p24 -> 1
     * 1080p25 -> 2
     * 1080p50 -> 3
     * 720p50  -> 4
     * 720p60  -> 5
     * ***************************/
    public void setResolution(int resolutionCode)
    {
        changingResolution = true;

        switch (resolutionCode)
        {
            case 1: case 2: case 3: cameraWidth = 1920; cameraHeight = 1080; break;
            case 4: case 5: cameraWidth = 1280; cameraHeight = 720; break;
        }

        OFInterop.blackMagic_setResolution(resolutionCode);
        _texture.Resize(cameraWidth, cameraHeight, TextureFormat.RGBA32, false);



        changingResolution = false;
    }
}
