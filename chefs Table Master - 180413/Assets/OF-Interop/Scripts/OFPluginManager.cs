using UnityEngine;
using System;
using System.Runtime.InteropServices;

internal static class OFInterop
{
    [DllImport("flyCaptureCvPlugin")]
    internal static extern bool flyCaptureCV_Setup();

    [DllImport("flyCaptureCvPlugin")]
    internal static extern bool flyCaptureCV_Update();

    [DllImport("flyCaptureCvPlugin")]
    internal static extern bool flyCaptureCV_resetBackground();

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_getCameraSettings(ref int width, ref int height);

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_setCameraSettings(ref int width, ref int height, ref int offsetX, ref int offsetY, ref int pixelFormat);

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_getContours(CvCircle* outObjects, int maxObjectsCount, ref int objectsCount);

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_getGrayDiff(byte* rawImage);

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_getGrayBlur(byte* rawBlurImage);

    [DllImport("flyCaptureCvPlugin")]
    internal unsafe static extern bool flyCaptureCV_setCVParameters(float threshold, float minSize, float maxSize, float blurAmount);

    [DllImport("blackMagicPlugin")]
    internal static extern bool blackMagic_Setup();

    [DllImport("blackMagicPlugin")]
    internal static extern bool blackMagic_Update();

    [DllImport("blackMagicPlugin")]
    internal unsafe static extern bool blackMagig_getImage(byte* blackMagicImage);

    [DllImport("blackMagicPlugin")]
    internal unsafe static extern bool blackMagic_setResolution(int resolution);

}

[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct CvCircle
{
    public int X, Y, Radius;
}


public class OFPluginManager : MonoBehaviour {

    //DLL Communication
    private int maxObjectsCount = 4;
    private int bytesPerPixel = 3;

    private int objectsCount = 0;
    private int cameraHeight = 0;
    private int cameraWidth = 0;

    private CvCircle[] _contours;
    private Texture2D _texture;
    private byte[] _processedImage;

    //Public properties
    public int resolutionWidth = 1288;
    public int resolutionHeight = 964;
    public int offsetX = 0;
    public int offsetY = 0;
    public int pixFormat = 0;

    [Range(0, 255f)]
    public float threshold = 80f;
    [Range(0, 100f)]
    public float minSize = 10000f;
    [Range(0, 150000f)]
    public float maxSize = 150000f;

    private float blurAmount = 0f;

    [Range(0, 20F)]
    public float xValueRange = 9F;
    [Range(0, 12F)]
    public float yValueRange = 5F;

    public GameObject[] colliders;

    [SerializeField]
    public float Threshold
    {
        get { return threshold; }
        set { threshold = value; }
    }

    [SerializeField]
    public float MinSize
    {
        get { return minSize; }
        set { minSize = value; }
    }

    [SerializeField]
    public float MaxSize
    {
        get { return maxSize; }
        set { maxSize = value; }
    }

    public int ObjectsCount
    {
        get  { return objectsCount;  }
        set  { objectsCount = value; }
    }

    void Start () {

        unsafe
        {
            //0 for RGB, 1 for MONO
            OFInterop.flyCaptureCV_setCameraSettings(ref resolutionWidth, ref resolutionHeight, ref offsetX, ref offsetY, ref pixFormat);
        }
        OFInterop.flyCaptureCV_Setup();
        Renderer _renderer = GetComponent<Renderer>();

        unsafe
        {
            OFInterop.flyCaptureCV_getCameraSettings(ref cameraWidth, ref cameraHeight);
            OFInterop.flyCaptureCV_setCVParameters(threshold, minSize, maxSize, blurAmount);
            //Debug.Log(threshold + "," + minSize + "," + maxSize);
        }

        _contours = new CvCircle[maxObjectsCount]; 
        //_texture = new Texture2D(cameraWidth, cameraHeight, TextureFormat.R8, false);
        _texture = new Texture2D(resolutionWidth, resolutionHeight, TextureFormat.R8, false);
        _renderer.material.mainTexture = _texture;
        cleanObjectsVector();
        resetColliders();

    }

    void Update() {

        OFInterop.flyCaptureCV_setCVParameters(threshold, minSize, maxSize, blurAmount);
        //Debug.Log(threshold + "," + minSize + "," + maxSize);

        OFInterop.blackMagic_Update();
        OFInterop.flyCaptureCV_Update();
        cleanObjectsVector();
        resetColliders();
        ObjectsCount = 0;
        unsafe
        {
            fixed (CvCircle* outObjects = _contours)
            {
                OFInterop.flyCaptureCV_getContours(outObjects, maxObjectsCount, ref objectsCount);
            }
        }

        Debug.Log(objectsCount);
        for (int i = 0; i < objectsCount; i++)
        {
           // Debug.Log(_contours[i].X + "," + _contours[i].Y + "," + _contours[i].Radius);
            float xValue = scale(0F , cameraWidth, -xValueRange, xValueRange, _contours[i].X );
            float zValue = scale(0F, cameraHeight, yValueRange, -yValueRange, _contours[i].Y );
            //Debug.Log(xValue + "," + zValue);
            colliders[i].transform.position = new Vector3(xValue, -0.3f, zValue);
        }
   
        _processedImage = null;
        _processedImage = new byte[cameraWidth * cameraHeight * bytesPerPixel];
        unsafe
        {
            fixed (byte* image = _processedImage)
            {
                OFInterop.flyCaptureCV_getGrayDiff(image);
            }
        }
        _texture.LoadRawTextureData(_processedImage);
        _texture.Apply();
    }

    public void resetBackground()
    {
        OFInterop.flyCaptureCV_resetBackground();
        Debug.Log("ResetBackground");
    }

    public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }

    public void cleanObjectsVector()
    {
        for (int i = 0; i < maxObjectsCount; i++)
        {
            _contours[i].X = 0;
            _contours[i].Y = 0;
            _contours[i].Radius = 0;
        }
    }

    public void resetColliders()
    {
        for (int i = 0; i < maxObjectsCount; i++)
        {
            colliders[i].transform.position = new Vector3(-1000, -0.3f, -1000);
        }
    }
}
