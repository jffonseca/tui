using UnityEngine;
using System;
//using System.Runtime.InteropServices;

public class OFPluginManagerSimple : MonoBehaviour {

    //DLL Communication
    private int maxObjectsCount = 4;
    private int bytesPerPixel = 3;

    private int objectsCount = 0;
    private int cameraHeight = 0;
    private int cameraWidth = 0;

    private CvCircle[] _contours;
    private Texture2D _texture;
    private byte[] _processedImage;
    public RenderTexture _renderTexture;

    //Public properties
    public int resolutionWidth = 1288;
    public int resolutionHeight = 964;
    public int offsetX = 0;
    public int offsetY = 0;
    public int pixFormat = 0;

    [Range(0, 255f)]
    public float threshold = 80f;
    [Range(0, 100f)]
    public float minSize = 10000f;
    [Range(0, 150000f)]
    public float maxSize = 150000f;

    [Range (0, 100f)]
    public float blurAmount = 20f;

    [Range(0, 20F)]
    public float xValueRange = 9F;
    [Range(0, 12F)]
    public float yValueRange = 5F;

    //public GameObject[] colliders;

    [SerializeField]
    public float Threshold
    {
        get { return threshold; }
        set { threshold = value; }
    }

    [SerializeField]
    public float MinSize
    {
        get { return minSize; }
        set { minSize = value; }
    }

    [SerializeField]
    public float MaxSize
    {
        get { return maxSize; }
        set { maxSize = value; }
    }

    public int ObjectsCount
    {
        get  { return objectsCount;  }
        set  { objectsCount = value; }
    }

    void Start () {

        unsafe
        {
            //0 for RGB, 1 for MONO
            OFInterop.flyCaptureCV_setCameraSettings(ref resolutionWidth, ref resolutionHeight, ref offsetX, ref offsetY, ref pixFormat);
        }
        OFInterop.flyCaptureCV_Setup();
        Renderer _renderer = GetComponent<Renderer>();

        unsafe
        {
            OFInterop.flyCaptureCV_getCameraSettings(ref cameraWidth, ref cameraHeight);
            OFInterop.flyCaptureCV_setCVParameters(threshold, minSize, maxSize, blurAmount);
            //Debug.Log(threshold + "," + minSize + "," + maxSize);
        }

        
        _contours = new CvCircle[maxObjectsCount]; 
        //_texture = new Texture2D(cameraWidth, cameraHeight, TextureFormat.R8, false);
        _texture = new Texture2D(resolutionWidth, resolutionHeight, TextureFormat.R8, false);
        _renderer.material.mainTexture = _texture;
        

    }

    void Update() {


        OFInterop.flyCaptureCV_setCVParameters(threshold, minSize, maxSize, blurAmount);

        OFInterop.flyCaptureCV_Update();
   
        _processedImage = null;
        _processedImage = new byte[cameraWidth * cameraHeight * bytesPerPixel];
        unsafe
        {
            fixed (byte* image = _processedImage)
            {
                OFInterop.flyCaptureCV_getGrayBlur(image);
            }
        }
        _texture.LoadRawTextureData(_processedImage);
        _texture.Apply();
        Graphics.Blit(_texture, _renderTexture);

    }

    public void resetBackground()
    {
        OFInterop.flyCaptureCV_resetBackground();
        Debug.Log("ResetBackground");
    }

    public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }
   
}
