﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class tvtCloner : MonoBehaviour {

    public GameObject prefab;
    public Vector2 number = new Vector2(1, 1);
    public Vector2 spacing = new Vector2(1, 1);
    public Vector2 offset = new Vector2(1, 1);

	void Start () {

        for (int y = 0; y < number.y; y++)
        {
            for (int x = 0; x < number.x; x++)
            {
                Vector3 pos = new Vector3(x * spacing.x + offset.x, 0, y * spacing.y + offset.y);
                Instantiate(prefab, pos, Quaternion.identity);
            }
        }

	}
	
}
