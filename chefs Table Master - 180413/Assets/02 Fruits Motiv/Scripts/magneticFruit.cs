﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class magneticFruit : MonoBehaviour {

	//public
	Vector3 newPosition;

	//private
	Transform trans;
	Transform originalPos;

	void Awake(){

		//the pos and rot to work with
		trans = transform;
		//hold the original pos and rot to come back to it.
		originalPos = transform;
	}

	void Update(){
		
		//move to the desired (steer) location

		//if we are close enough, just dont move anymore

	}

	void OnTriggerEnter(Collider other){

		if (other.tag == "Attractor") {
			//get the reference to the position to lerp in the update (minus the collider radius?)
		}
	}


	void OnTriggerExit(Collider other){
		if (other.tag == "Attractor") {
			//start to move to the original position
		}
	}

}
