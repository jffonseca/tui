﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleVisible : MonoBehaviour {

    public GameObject[] objectsArray;
    public bool activeOnStart = false;
	// Use this for initialization
	public void toggle () {
        activeOnStart = !activeOnStart;
        for (int i = 0; i<objectsArray.Length; i++)
        {
          objectsArray[i].SetActive(activeOnStart);
        }
     }
}
