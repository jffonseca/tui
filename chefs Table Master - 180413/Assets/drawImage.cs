﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawImage : MonoBehaviour {


    public Texture aTexture;

	// Use this for initialization
	void onGUI () {

        GUI.DrawTexture(new Rect(10, 10, 600, 600), aTexture, ScaleMode.ScaleToFit, true, 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
