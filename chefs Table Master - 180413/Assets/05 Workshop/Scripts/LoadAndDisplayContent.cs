﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;




public class LoadAndDisplayContent : MonoBehaviour
{

    public enum Workshop
    {
       
        Cocktail,
        Einkochen,
        Fleisch,
        General,
        Pasta,
        Schokoladen,
        Sushi,
        Whisky,
        Workshop,
        Chs,
        Win

    }


    public Workshop workshopIndex;


    private string wksAnimatedPath = "NMS2_WORKSHOPS/WANIMATED";
    private string wksStaticPath = "NMS2_WORKSHOPS/WSTATIC";

   // private Dictionary<string, SlideShow> WKS;
    private List< SlideShow> WKS;
    private int ActiveWorkshopIndex=0;

    //Raw Image to Show Video Images [Assign from the Editor]
    public RawImage image;
    public Text DebugText;


    void Start()
    {


        
        ActiveWorkshopIndex = (int)workshopIndex;
        //  WKS = new Dictionary<string, SlideShow>();
        WKS = new List<SlideShow>();
        int wksId = 0;

        //LOAD STATIC WORKSHOPS PATHS

     
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, wksStaticPath);

        var info = new DirectoryInfo(filePath); //.Replace("/", "\\")
        var fileInfo = info.GetDirectories();
        for (int i = 0; i < fileInfo.Length; i++)
        {
            string[] str = fileInfo[i].ToString().Split('\\');
            string nam = str[str.Length - 1];
            // Debug.Log(nam);
            // WKS.Add(nam, new SlideShow(wksId, nam, 1, fileInfo[i].ToString()));
            WKS.Add(new SlideShow(this, wksId, nam, 0, fileInfo[i].ToString(), ref image));
            wksId++;
        }


        filePath = System.IO.Path.Combine(Application.streamingAssetsPath, wksAnimatedPath);

        //LOAD ANIMATED WORKSHOPS PATHS
        info = new DirectoryInfo(filePath);
        fileInfo = info.GetDirectories();



    

        for (int i = 0; i < fileInfo.Length; i++)
        {
            string[] str = fileInfo[i].ToString().Split('\\');
            string nam = str[str.Length - 1];
            //  Debug.Log(nam);
            //  WKS.Add(nam, new SlideShow(wksId, nam, 1, fileInfo[i].ToString()));
            WKS.Add(new SlideShow(this, wksId, nam, 1, fileInfo[i].ToString(), ref image));
            wksId++;
        }

      
        

        WKS[ActiveWorkshopIndex].LoadContent();

       
    }



    // Update is called once per frame 
    void Update()
    {

        if (ActiveWorkshopIndex != (int)workshopIndex)
        {
            Debug.Log("Unloading Content");
            WKS[ActiveWorkshopIndex].UnloadContent();
            ActiveWorkshopIndex = (int)workshopIndex;
            Debug.Log("Loading New Content From " + workshopIndex);

            WKS[ActiveWorkshopIndex].LoadContent();     
        }




        if (WKS[ActiveWorkshopIndex].type == 1) {

            WKS[ActiveWorkshopIndex].Update();
        }




        UpdateDebugText();
    }



    void UpdateDebugText() {


        if (!WKS[ActiveWorkshopIndex].StartAnimationWorkshop) DebugText.text = "Title";
        else {

            if (WKS[ActiveWorkshopIndex].videoPlayerList[WKS[ActiveWorkshopIndex].videoIndex].isLooping)
            {
                int index = -1;

                for (int i = 0; i < WKS[ActiveWorkshopIndex].loopIndex.Count; i++) {


                    if (WKS[ActiveWorkshopIndex].videoIndex == WKS[ActiveWorkshopIndex].loopIndex[i]) index = i;
                }
                DebugText.text = "Slide " + index;
            }
            else {
                DebugText.text = "Transition";
            }




        }

    }




    public void resetWorkshop()
    {

        if (WKS[ActiveWorkshopIndex].type == 1)
        {
            WKS[ActiveWorkshopIndex].resetWorkshop();
        }
    }


    public void nextSlide()
    {

        if (WKS[ActiveWorkshopIndex].type == 1)
        {
            WKS[ActiveWorkshopIndex].NextClip();
        }
    }



    public void previousSlide()
    {

        if (WKS[ActiveWorkshopIndex].type == 1)
        {
            WKS[ActiveWorkshopIndex].PreviousClip();
        }
    }



    void OnGUI()
    {
        
        if (WKS[ActiveWorkshopIndex].type == 0)
        {
            image.texture = WKS[ActiveWorkshopIndex].slides[WKS[ActiveWorkshopIndex].StaticIndex];

            // GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), WKS[ActiveWorkshopIndex].slides[0], ScaleMode.ScaleToFit); // ScaleMode.StretchToFill
        }
        else
        {
            if (!WKS[ActiveWorkshopIndex].StartAnimationWorkshop)
            {
                image.texture = WKS[ActiveWorkshopIndex].AnimatedWksSplashScreen;
            }


        }
        
    }




    public void upateWorkshopIndexFromOSC(int _index) {
        workshopIndex = (Workshop)_index;
    }

    
}
