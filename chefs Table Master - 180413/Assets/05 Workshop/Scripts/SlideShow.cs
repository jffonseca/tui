﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video;
using UnityEngine.UI;
//using System.Runtime.InteropServices;

public class SlideShow : MonoBehaviour {


    public int id;
    public int type; // 0 - Static !! 1 - Animated
    public string nam;
    public string path;
    //Raw Image to Show Video Images [Assign from the Editor]
    public RawImage image;

    public List<Texture2D> slides;



    //FOR VIDEO
    //Set from the Editor
    List<string> videoClipList;

    public List<VideoPlayer> videoPlayerList;
    public List<GameObject> vidHolder;
    public Texture2D AnimatedWksSplashScreen;


    public int StaticIndex = 0;
    public int videoIndex = 0;
    private int lastIndex = 0;
    private int nextIndex = 0;


    bool currentClipIsLooping;
    bool PreparetoJumpNextClip = false;

    public List<int> loopIndex;

    MonoBehaviour mono;

    public bool StartAnimationWorkshop;


    //[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    //public static extern void OutputDebugString(string message);

    public SlideShow(MonoBehaviour _mono, int _id, string _nam, int _type, string _path , ref RawImage _image) {


 
        mono = _mono;

    //    Debug.Log("SlideShow Path: " + _path);
        id = _id;
        nam = _nam;
        type = _type;
        path = _path;
        image = _image;

       

    }

    public void LoadContent()
    {
        if (type == 0)
        {
            LoadStaticContent();
        }
        else
        {
            LoadAnimationContent();
        }
    }

    public void UnloadContent()
    {

        if (type == 0)
        {
            UnloadStaticContent();
        }
        else
        {
            UnloadAnimationContent();
        }
    }


    //STATIC CONTENT
    //___________________________________________________________________________________________
    void LoadStaticContent() {



     StaticIndex = 0;
        //Debug.Log("loading...");
        slides = new List<Texture2D>();
        //
        var info = new DirectoryInfo(path);
        var fileInfo = info.GetFiles();

        for (int i = 0; i < fileInfo.Length; i++) {

            //Check if png 
            string[] str = fileInfo[i].ToString().Split('.');
            string fileType = str[str.Length - 1];

            string[] strLastName = fileInfo[i].ToString().Split('\\');
            string[] strInit = strLastName[strLastName.Length-1].Split('_');
            string fileInit = strInit[0];
            //Debug.Log(fileInit);
            if (fileType == "png" && fileInit == "WS") {
                string file = path + "\\" + strLastName[strLastName.Length - 1];
                Debug.Log(file);
                byte[] fileData = File.ReadAllBytes(file);
                slides.Add(new Texture2D(2,2));
                slides[slides.Count - 1].LoadImage(fileData);
            }
        }
    }

    void UnloadStaticContent() {
        slides.Clear();
    }

    //ANIMATION CONTENT
    //___________________________________________________________________________________________
    void LoadAnimationContent()
    {

        StartAnimationWorkshop = false;
        loadSpashScreen();

        videoIndex = 0;
        loopIndex = new List<int>();
        currentClipIsLooping = false;
        mono.StartCoroutine(playVideo());

        setLoopVideos();
        //PlayClip();
    }

    void UnloadAnimationContent()
    {
        mono.StopAllCoroutines();
     


        videoPlayerList[videoIndex].Stop();



        for (int i = 0; i < videoPlayerList.Count; i++) {
            DestroyObject(videoPlayerList[i]);
            DestroyObject(vidHolder[i]);
            Debug.Log("Destroying obj: " + i);
        }
    
    }
    //___________________________________________________________________________________________

    public void resetWorkshop() {

        UnloadAnimationContent();
        LoadAnimationContent();

    }



    // Update is called once per frame
    public void Update() {


        //FOR VIDEOS
        if (type == 1) {

          
            

            if (PreparetoJumpNextClip && isCurrentVideoLooping() && videoPlayerList[videoIndex].frame >= (long)videoPlayerList[videoIndex].frameCount - 1)
            {
                //videoPlayerList[videoIndex].isLooping = false;
              //  videoPlayerList[videoIndex].Stop();
                PlayNextClip();
                PreparetoJumpNextClip = false;
            }
            

       //     Debug.Log(videoPlayerList[videoIndex].frame + " / " + (long)videoPlayerList[videoIndex].frameCount + " index: "+ videoIndex);

            if (videoPlayerList[videoIndex].isPrepared && !isCurrentVideoLooping() && videoPlayerList[videoIndex].frame >= (long)videoPlayerList[videoIndex].frameCount - 1)
            {
              //  Debug.Log("Current video " + videoIndex + " is looping: " + currentClipIsLooping);
                PlayNextClip();

            }


        }
    }



    public void NextClip()
    {

        if(type == 0){

            StaticIndex++;
            StaticIndex = (int)Mathf.Repeat(StaticIndex, slides.Count);
        }
        else{

            if (!StartAnimationWorkshop)
            {
                StartAnimationWorkshop = true;
            }
            else {
                if (videoPlayerList[videoIndex].isLooping && !PreparetoJumpNextClip) PreparetoJumpNextClip = true;
            }

        }
    }



    public void PreviousClip()
    {



        if (type == 0)
        {

            StaticIndex--;
            StaticIndex = (int)Mathf.Repeat(StaticIndex, slides.Count);
        }
        else
        {

            if (videoIndex > loopIndex[0])
            {

                for (int i = loopIndex.Count - 1; i >= 0; i--)
                {

                    //     Debug.Log("index: "+i + " loopIndex[i]: "+ loopIndex[i] + " videoIndex: "+ videoIndex);
                    if (loopIndex[i] < videoIndex)
                    {


                        // videoPlayerList[videoIndex].Stop();
                        PreparetoJumpNextClip = false;

                        lastIndex = videoIndex;
                        videoIndex = loopIndex[i];
                        videoIndex = videoIndex % videoClipList.Count;

                        Debug.Log("Going back to Clip: " + videoIndex);
                        //    mono.StopAllCoroutines();
                        mono.StartCoroutine(playVideo(false));
                        setLoopVideos();
                        break;

                    }


                }

            }
        }
    }


    public void setLoopVideos()
    {

        for (int i = 0; i < loopIndex.Count; i++)
        {
            videoPlayerList[loopIndex[i]].isLooping = true;
        }
    }


    bool isCurrentVideoLooping()
    {

        bool loop = false;
        for (int i = 0; i < loopIndex.Count; i++)
        {
            if (loopIndex[i] == videoIndex)
            {
                loop = true;
                break;
            }
        }
        return loop;
    }


    void PlayNextClip()
    {



        lastIndex = videoIndex;
        lastIndex = lastIndex % videoClipList.Count;
        videoIndex++;
        if (videoIndex > videoPlayerList.Count - 1)
        {
            Debug.Log("should stop here");
            StartAnimationWorkshop = false;
        }
        videoIndex = videoIndex % videoClipList.Count;

        //Play next prepared video. Pass false to it so that some codes are not executed at-all
        mono.StartCoroutine(playVideo(false));
        setLoopVideos();
    }


    void loadSpashScreen() {

        var info = new DirectoryInfo(path);
        var fileInfo = info.GetFiles();

        for (int i = 0; i < fileInfo.Length; i++)
        {

            //Check if mp4 
            string[] str = fileInfo[i].ToString().Split('.');
            string fileType = str[str.Length - 1];



            string[] strLastName = fileInfo[i].ToString().Split('\\');
            string[] strInit = strLastName[strLastName.Length - 1].Split('.');
            string fileInit = strInit[0];

            if (fileType == "png" && fileInit == "WA_TITLE")
            {
                string file = path + "\\" + strLastName[strLastName.Length - 1];
              //  Debug.Log(file);
             

                byte[] fileData = File.ReadAllBytes(file.Replace("/", "\\"));
                AnimatedWksSplashScreen = new Texture2D(2, 2);
                AnimatedWksSplashScreen.LoadImage(fileData);
            }
        }
    }

     IEnumerator playVideo(bool firstRun = true)
    {
       
        //Init videoPlayerList first time this function is called
        if (firstRun)
        {

            videoClipList = new List<string>();

            var info = new DirectoryInfo(path);
            var fileInfo = info.GetFiles();
     
            for (int i = 0; i < fileInfo.Length; i++)
            {

                //Check if mp4 
                string[] str = fileInfo[i].ToString().Split('.');
                string fileType = str[str.Length - 1];

                string[] strLastName = fileInfo[i].ToString().Split('\\');
                string[] strInit = strLastName[strLastName.Length - 1].Split('_');
                string fileInit = strInit[0];
                //Debug.Log(fileInit);

     
                if (fileType == "mp4" && fileInit == "WA")
                {

                    string file = path + "\\" + strLastName[strLastName.Length - 1];
                 //   OutputDebugString(file);

                    Debug.Log(file);
                    videoClipList.Add(file);
                }
            }



            videoPlayerList = new List<VideoPlayer>();
            vidHolder = new List<GameObject>();

            for (int i = 0; i < videoClipList.Count; i++)
            {
                //Create new Object to hold the Video and the sound then make it a child of this object
                GameObject VH = new GameObject("VP" + i);
                vidHolder.Add(VH);
                //vidHolder.transform.SetParent(transform);

                //Add VideoPlayer to the GameObject
                VideoPlayer videoPlayer = VH.AddComponent<VideoPlayer>();
                videoPlayerList.Add(videoPlayer);


                //CHECK IF VIDEO IS LOOPABLE
               // Debug.Log(videoClipList[i]);

                string[] strs = videoClipList[i].Split('\\');
                string fileName = strs[strs.Length-1];

                if (fileName.Contains("LOOP")) loopIndex.Add(i);

                //Disable Play on Awake for both Video and Audio
                videoPlayer.playOnAwake = false;
                //videoPlayer.waitForFirstFrame = true;

                //We want to play from video clip not from url
                videoPlayer.url = videoClipList[i];//VideoSource.VideoClip;

            }
        }

        /*
        //Make sure that the NEXT VideoPlayer index is valid
        if (videoIndex >= videoPlayerList.Count)
            yield break;
        */

        //Prepare video
        videoPlayerList[videoIndex].Prepare();

        //Wait until this video is prepared
        while (!videoPlayerList[videoIndex].isPrepared)
        {
          //  Debug.Log("Preparing Index: " + videoIndex);
            yield return null;
        }
       // Debug.LogWarning("Done Preparing current Video Index: " + videoIndex);

        currentClipIsLooping = videoPlayerList[videoIndex].isLooping;


        if(!StartAnimationWorkshop){
            while (!StartAnimationWorkshop) {
                yield return null;
            }
        }

        //Play  video
     //   Debug.LogWarning("Playing video " + videoIndex + "loop: "+ videoPlayerList[videoIndex].isLooping);
        videoPlayerList[videoIndex].Play();


        while (videoPlayerList[videoIndex].frame < 2)
        {
            yield return null;
        }
        yield return null;

        //Assign the Texture from Video to RawImage to be displayed

        image.texture = videoPlayerList[videoIndex].texture;

        if (!firstRun)
        {
            videoPlayerList[lastIndex].Stop();
        }

        //Wait while the current video is playing
        bool reachedHalfWay = false;
        nextIndex = (videoIndex + 1) % videoClipList.Count;
        while (videoPlayerList[videoIndex].isPlaying)
        {

          //  Debug.Log("Playing time: " + videoPlayerList[videoIndex].time + " INDEX: " + videoIndex);

            //(Check if we have reached half way)
            if (!reachedHalfWay && videoPlayerList[videoIndex].frame >= (long)videoPlayerList[videoIndex].frameCount/2)
            {
                reachedHalfWay = true; //Set to true so that we don't evaluate this again

           

                //Prepare the NEXT video
             //   Debug.LogWarning("Ready to Prepare NEXT Video Index: " + nextIndex);
                videoPlayerList[nextIndex].Prepare();
            }
            
            yield return null;
        }
      //  Debug.Log("Done Playing current Video Index: " + videoIndex);

        //Wait until NEXT video is prepared
       
        /*
        while (!videoPlayerList[nextIndex].isPrepared)
        {
            //  Debug.Log("Preparing NEXT Video Index: " + nextIndex);
            yield return null;
        }
        */


     //   PlayNextClip();
    }
}
