﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Display : MonoBehaviour
{
    public RawImage image;
    // Use this for initialization
    void Start ()
    {

       Rect rect = image.rectTransform.rect;

        rect.width = Screen.width;
        rect.height = Screen.height;

    }
}
