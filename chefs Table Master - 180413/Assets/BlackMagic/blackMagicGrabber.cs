﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackMagicGrabber : MonoBehaviour {

    //public string WD_camera;
    public int FPS = 30;
    public Vector2 resolution = new Vector2(1920, 1080);
    public WebCamTexture webcamTexture;

    // Use this for initialization
    void Start () {

        WebCamDevice[] devices = WebCamTexture.devices;
        Renderer _renderer = GetComponent<Renderer>();


        bool found = false;

        //for (int i = 0; i < devices.Length; i++)
        //{

            //Debug.Log("camera " + i + ": " + devices[i].name);

            //if (devices[i].name == WD_camera)
            //{
                webcamTexture = new WebCamTexture(devices[0].name, (int)resolution.x, (int)resolution.y, FPS);
                Debug.Log("chosen camera: " + devices[0].name);
                _renderer.material.mainTexture = webcamTexture;
               // webcamTexture.requestedFPS = 60;
               // webcamTexture.requestedWidth = 1280;
               // webcamTexture.requestedHeight = 720;
                webcamTexture.Play();
                Debug.Log(webcamTexture.isPlaying);
                Debug.Log("width: " + webcamTexture.width);
                Debug.Log("height: " + webcamTexture.height);
                found = true;
                //break;
            //}

        //}

        if (!found)
        {
            //Debug.Log("Required Camera (" + WD_camera + ") not present");
        }

    }

	// Update is called once per frame
	void Update () {

	}
}
