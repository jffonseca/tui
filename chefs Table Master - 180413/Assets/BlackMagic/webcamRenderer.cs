﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class webcamRenderer : MonoBehaviour {

    public string WD_camera = "";
    public int FPS = 30;
    public Vector2 resolution = new Vector2(1280, 720);
    //public RenderTexture renderTexture;
    private WebCamTexture webcamTexture;
    public Material material;

    // Use this for initialization
    void Start () {

        WebCamDevice[] devices = WebCamTexture.devices;
       

        bool found = false;

        for (int i = 0; i < devices.Length; i++)
        {

            Debug.Log("camera " + i + ": " + devices[i].name);

            if (devices[i].name == WD_camera)
            {
                webcamTexture = new WebCamTexture(devices[i].name, (int)resolution.x, (int)resolution.y, FPS);
                Debug.Log("chosen camera: " + devices[i].name);
                material.mainTexture = webcamTexture;
                webcamTexture.Play();
                Debug.Log(webcamTexture.isPlaying);
                Debug.Log("width: " + webcamTexture.width);
                Debug.Log("height: " + webcamTexture.height);
                found = true;
                break;
            }

        }

        if (!found)
        {
            Debug.Log("Required Camera (" + WD_camera + ") not present");
        }

    }
	
	// Update is called once per frame
	//void Update () {
		
	//}
}
