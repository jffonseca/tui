﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sceneController : MonoBehaviour {

    //public string[] scenes;
    public Color loadToColor = Color.black;
    public float fadeTime = 0.5f;
    private int sceneIndex;

    // The scenes need to be added in the Build Settings dialog for this to work
    public void changeScene(int index)
    {
      sceneIndex = index;
      Debug.Log("changeScene: " + sceneIndex);
      SceneChanger.Fade(sceneIndex, loadToColor, fadeTime);
    }
}
