﻿using System.Collections;
using UnityEngine;

public static class SceneChanger {

    //Create Fader object and assing the fade scripts and assign all the variables
    public static void Fade(string scene, Color col, float damp)
    {
        GameObject changer = new GameObject();
        changer.name = "Fader";
        changer.AddComponent<Fader>();
        Fader scr = changer.GetComponent<Fader>();
        scr.fadeDamp = damp;
        scr.fadeScene = scene;
        scr.fadeColor = col;
        scr.start = true;
    }

    public static void Fade(int sceneIndex, Color col, float damp)
    {
        GameObject changer = new GameObject();
        changer.name = "Fader";
        changer.AddComponent<Fader>();
        Fader scr = changer.GetComponent<Fader>();
        scr.fadeDamp = damp;
        scr.fadeSceneIndex = sceneIndex;
        scr.fadeColor = col;
        scr.start = true;
    }
}
